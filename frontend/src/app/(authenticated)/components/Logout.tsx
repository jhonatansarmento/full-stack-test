'use client';
import LogoutIcon from '@mui/icons-material/ExitToApp';
import { Box, Button } from '@mui/material';
import { useRouter } from 'next/navigation';
[];
const Logout = () => {
  const router = useRouter();
  const handleLogout = () => {
    let confirmed = null;
    if (typeof window !== 'undefined') {
      confirmed = window.confirm('Tem certeza que deseja fazer logout?');
    }
    if (confirmed) {
      if (typeof window !== 'undefined') {
        localStorage.removeItem('token');
        router.push('/login');
      }
    }
  };

  return (
    <Button color='inherit' onClick={handleLogout}>
      <Box
        display='flex'
        flexDirection='row'
        gap={1}
        alignItems='center'
        justifyContent='center'
      >
        <LogoutIcon fontSize='small' />
        Sair
      </Box>
    </Button>
  );
};

export default Logout;
