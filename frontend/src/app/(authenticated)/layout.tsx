import '@/app/globals.css';
import { AppBar, Grid, Toolbar } from '@mui/material';
import { Metadata } from 'next';
import { Inter } from 'next/font/google';
import Logout from './components/Logout';
import NavLink from './components/NavLink';

const inter = Inter({ subsets: ['latin'] });

export const metadata: Metadata = {
  title: 'Desafio FullStack',
  description: 'Generated by create next app',
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang='pt-br'>
      <body className={inter.className}>
        <header>
          <nav>
            <AppBar position='static'>
              <Toolbar>
                <Grid container gap={2}>
                  <NavLink href='/home'>Pagina Inicial</NavLink>
                  <NavLink href='/movies'>Filmes</NavLink>
                  <NavLink href='/users'>Usuários</NavLink>
                </Grid>
                <Logout />
              </Toolbar>
            </AppBar>
          </nav>
        </header>
        <main>{children}</main>
      </body>
    </html>
  );
}
