import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Toolbar,
} from '@mui/material';
import Pagination from '@mui/material/Pagination';
import React, { useState } from 'react';

interface User {
  id: number;
  name: string;
  email: string;
  password: string;
}

interface UserTableProps {
  data: User[];
}

const UserTable: React.FC<UserTableProps> = ({ data }) => {
  const [page, setPage] = useState(1);
  const rowsPerPage = 5;
  const handleChangePage = (
    event: React.ChangeEvent<unknown>,
    newPage: number
  ) => {
    setPage(newPage);
  };

  const firstItemIndex = (page - 1) * rowsPerPage;
  const lastItemIndex = page * rowsPerPage;

  return (
    <div>
      <main>
        <Toolbar>
          <TableContainer component={Paper}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell style={{ fontWeight: 'bold', width: '50%' }}>
                    Nome
                  </TableCell>
                  <TableCell
                    style={{ fontWeight: 'bold', width: '25%' }}
                    align='left'
                  >
                    Email
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {data.slice(firstItemIndex, lastItemIndex).map((user: User) => (
                  <TableRow key={user.id}>
                    <TableCell
                      component='th'
                      scope='row'
                      style={{ width: '50%' }}
                    >
                      {user.name}
                    </TableCell>
                    <TableCell align='left' style={{ width: '25%' }}>
                      {user.email}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Toolbar>

        <div
          style={{
            width: '100%',
            display: 'flex',
            justifyContent: 'center',
            marginTop: '20px',
          }}
        >
          <Pagination
            count={Math.ceil(data.length / rowsPerPage)}
            page={page}
            onChange={handleChangePage}
            shape='rounded'
          />
        </div>
      </main>
    </div>
  );
};

export default UserTable;
