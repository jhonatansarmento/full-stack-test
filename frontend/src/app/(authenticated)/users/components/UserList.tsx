'use client';
import api from '@/shared/services/api';
import { Toolbar } from '@mui/material';
import { useRouter } from 'next/navigation';
import React, { useEffect, useState } from 'react';
import UserTable from './userTable';
interface User {
  id: number;
  name: string;
  email: string;
  password: string;
}

const Users: React.FC = () => {
  const [data, setData] = useState<User[]>([]);
  const [firstRun, setFirstRun] = useState(true);

  const router = useRouter();

  const fetchData = async () => {
    let token = null;
    if (typeof window !== 'undefined') {
      token = localStorage.getItem('token');
    }
    if (!token) router.push('/login');
    try {
      const response = await api.get('/users', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      setData(response.data);
    } catch (error) {
      console.error('Erro ao buscar dados da API:', error);
    }
  };

  //eslint-disable-next-line
  useEffect(() => {
    if (firstRun) {
      fetchData();
      setFirstRun(false);
    }
  });

  return (
    <div>
      <main>
        <Toolbar>
          <h1>Lista de Usuários</h1>
        </Toolbar>
        <UserTable data={data} />
        <div
          style={{
            width: '100%',
            display: 'flex',
            justifyContent: 'center',
            marginTop: '20px',
          }}
        ></div>
      </main>
    </div>
  );
};

export default Users;
