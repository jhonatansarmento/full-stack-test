import UserList from '@/app/(authenticated)/users/components/UserList';
import React from 'react';

const User: React.FC = () => {
  return <UserList />;
};

export default User;
