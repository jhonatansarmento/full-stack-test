'use client';
import api from '@/shared/services/api';
import {
  Pagination,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Toolbar,
} from '@mui/material';
import { useRouter } from 'next/navigation';
import { useEffect, useState } from 'react';

const MOVIES_PER_PAGE = 5;

interface MovieProps {
  Title: string;
  Year: string;
  imdbID: string;
  Type: string;
  Poster: string;
}

const Movies: React.FC = () => {
  const [page, setPage] = useState(1);
  const [data, setData] = useState([]);
  const [firstRun, setFirstRun] = useState(true);

  const router = useRouter();

  const fetchData = async () => {
    let token = null;
    if (typeof window !== 'undefined') {
      token = localStorage.getItem('token');
    }
    if (!token) router.push('/login');
    try {
      const response = await api.get('/movies', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      setData(response.data);
    } catch (error) {
      console.error('Erro ao buscar dados da API:', error);
    }
  };

  //eslint-disable-next-line
  useEffect(() => {
    if (firstRun) {
      fetchData();
      setFirstRun(false);
    }
  });

  const handleChangePage = (
    event: React.ChangeEvent<unknown>,
    newPage: number
  ) => {
    setPage(newPage);
  };

  const indexOfLastMovie = page * MOVIES_PER_PAGE;
  const indexOfFirstMovie = indexOfLastMovie - MOVIES_PER_PAGE;
  const currentMovies = data.slice(indexOfFirstMovie, indexOfLastMovie);

  return (
    <div>
      <main>
        <Toolbar>
          <h1>Lista de Filmes</h1>
        </Toolbar>
        <Toolbar>
          <TableContainer component={Paper}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell style={{ fontWeight: 'bold', width: '50%' }}>
                    Título
                  </TableCell>
                  <TableCell
                    style={{ fontWeight: 'bold', width: '25%' }}
                    align='right'
                  >
                    Ano
                  </TableCell>
                  <TableCell
                    style={{ fontWeight: 'bold', width: '25%' }}
                    align='right'
                  >
                    Gênero
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {currentMovies.map((movie: MovieProps) => (
                  <TableRow key={movie.imdbID}>
                    <TableCell
                      component='th'
                      scope='row'
                      style={{ width: '50%' }}
                    >
                      {movie.Title}
                    </TableCell>
                    <TableCell align='right' style={{ width: '25%' }}>
                      {movie.Year}
                    </TableCell>
                    <TableCell align='right' style={{ width: '25%' }}>
                      {movie.imdbID}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Toolbar>

        <div
          style={{
            width: '100%',
            display: 'flex',
            justifyContent: 'center',
            marginTop: '20px',
          }}
        >
          <Pagination
            count={Math.ceil(data.length / MOVIES_PER_PAGE)}
            page={page}
            onChange={handleChangePage}
            shape='rounded'
          />
        </div>
      </main>
    </div>
  );
};

export default Movies;
