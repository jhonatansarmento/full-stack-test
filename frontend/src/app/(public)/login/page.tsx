'use client';
import api from '@/shared/services/api';
import { Box, Card, CardContent, Typography } from '@mui/material';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import { useRouter } from 'next/navigation';
import React, { useEffect, useState } from 'react';
import * as Yup from 'yup';
interface LoginFormValues {
  email: string;
  password: string;
}

const loginSchema = Yup.object().shape({
  email: Yup.string()
    .email('Digite um email válido.')
    .required('O e-mail do usuário é obrigatório.'),
  password: Yup.string()
    .min(5, 'A senha deve ter no mínimo 5 caracteres.')
    .required('A senha é obrigatória.'),
});

const Login: React.FC = () => {
  const [formValues, setFormValues] = useState<LoginFormValues>({
    email: '',
    password: '',
  });
  const [errors, setErrors] = useState<{ [key: string]: string }>({});
  const [hasError, setHasError] = useState(false);

  useEffect(() => {
    let token = null;
    if (typeof window !== 'undefined') {
      token = localStorage.getItem('token');
    }
    if (token) router.push('/home');
  });

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setFormValues((prevValues) => ({
      ...prevValues,
      [name]: value,
    }));
  };

  const router = useRouter();

  const isFormValid = formValues.email && formValues.password;

  const handleLogin = () => {
    loginSchema
      .validate(formValues, { abortEarly: false })
      .then(async () => {
        try {
          const response = await api.post('/login', {
            email: formValues.email,
            password: formValues.password,
          });
          const token = response.data.token;
          if (typeof window !== 'undefined') {
            localStorage.setItem('token', token);
          }
        } catch (error: any) {
          alert(error.response.data.message);
          console.error('Erro ao fazer login:', error);
        }

        setHasError(false);
        router.push('/home');
      })
      .catch((validationErrors) => {
        const newErrors: { [key: string]: string } = {};
        validationErrors.inner.forEach(
          (error: { path: string | number; message: string }) => {
            newErrors[error.path] = error.message;
          }
        );
        setErrors(newErrors);
        setHasError(true);
      });
  };

  return (
    <Box
      width='100vw'
      height='100vh'
      display='flex'
      alignItems='center'
      justifyContent='center'
    >
      <Card>
        <CardContent>
          <Box display='flex' flexDirection='column' gap={2} width={250}>
            <Typography variant='h6' align='center'>
              Fazer login
            </Typography>
            <TextField
              label='Email'
              variant='outlined'
              name='email'
              value={formValues.email}
              onChange={handleChange}
              error={Boolean(errors.email)}
              helperText={errors.email}
            />
            <TextField
              label='Password'
              type='password'
              variant='outlined'
              name='password'
              value={formValues.password}
              onChange={handleChange}
              error={Boolean(errors.password)}
              helperText={errors.password}
            />
            <Button
              fullWidth
              variant='contained'
              onClick={handleLogin}
              disabled={!isFormValid}
            >
              Login
            </Button>
          </Box>
        </CardContent>
      </Card>
    </Box>
  );
};

export default Login;
