import EditIcon from '@mui/icons-material/Edit';
import { Button, Tooltip } from '@mui/material';
import React from 'react';

interface EditButtonProps {
  onClick: () => void;
}

const EditButton: React.FC<EditButtonProps> = ({ onClick }) => {
  return (
    <Tooltip title='Editar' placement='top'>
      <Button
        variant='contained'
        color='primary'
        size='small'
        onClick={onClick}
      >
        <EditIcon />
      </Button>
    </Tooltip>
  );
};

export default EditButton;
