import DeleteIcon from '@mui/icons-material/Delete';
import { Button, Tooltip } from '@mui/material';
import React from 'react';

interface DeleteButtonProps {
  onClick: () => void;
}

const DeleteButton: React.FC<DeleteButtonProps> = ({ onClick }) => {
  return (
    <Tooltip title='Excluir' placement='top'>
      <Button variant='outlined' color='error' size='small' onClick={onClick}>
        <DeleteIcon />
      </Button>
    </Tooltip>
  );
};

export default DeleteButton;
