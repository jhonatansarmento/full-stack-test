<br>

<p align="center">

# Projeto de Aplicação Full Stack - Listagem de Usuários e Filmes

</p>

> Este é o README completo para o projeto de aplicação Full Stack, que consiste em criar uma aplicação com Backend utilizando Node.js que expõe uma API REST para listar usuários e filmes. Além disso, a aplicação web conterá uma interface desenvolvida com Next.js para autenticação e acesso aos dados de uma API externa.

<br>

### - API

> - Listagem de Usuários: A API REST permite listar todos os usuários cadastrados no sistema.
> - Listagem de Filmes: A API REST também fornece uma lista de filmes disponíveis.
> - Autenticação: Implementação de um mecanismo de autenticação seguro para acesso às rotas protegidas da API.

### - Frontend (Next.js)

> - Página de Login: Página de login onde os usuários podem inserir suas credenciais para acessar a aplicação.
> - Página de Listagem de Usuários: Página que exibe a lista de usuários cadastrados.
> - Página de Listagem de Filmes: Página que exibe a lista de filmes disponíveis.

<br>

## Pré-requisitos

- [node](https://nodejs.org/en/download/) >= 10.0.0
- [npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) >= 5.5.0

<br>

## Tecnologias Utilizadas

- O projeto foi desenvolvido utilizando as seguintes tecnologias:

- Node.js: Backend da aplicação foi construído utilizando Node.js, que é uma plataforma de desenvolvimento que permite utilizar JavaScript no servidor.
- Express: Framework Node.js utilizado para criação da API REST, que facilita a criação de rotas e endpoints.
- Next.js: Framework React utilizado para construção do frontend da aplicação, fornecendo renderização no lado do servidor e roteamento.
- React: Biblioteca JavaScript para construção de interfaces de usuário.
- JWT (JSON Web Tokens): Utilizado para autenticação dos usuários na API.

## ⚠️ Como executar o frontend

Siga os passos abaixo para executar a aplicação localmente:

- Clone este repositório para o seu computador:

```
git clone https://gitlab.com/jhonatansarmento/full-stack-test
cd frontend
```

- Instale as dependências do projeto:

```
npm install
```

- Inicie o servidor do frontend

```
npm run dev
```

## ⚠️ Como executar o backend

Siga os passos abaixo para executar a aplicação localmente:

- Clone este repositório para o seu computador:

```
git clone https://gitlab.com/jhonatansarmento/full-stack-test
cd backend
```

- Instale as dependências do projeto:

```
npm install
```

- Inicie o servidor do backend

```
npm run dev
```

# Acesse a aplicação no seu navegador através da URL: http://localhost:3000.

### Como usar a aplicação

- Acesse a página de login e insira suas credenciais para autenticação.

exemplo de uma credencial valida
clyde19@yahoo.com
k97crcKQo5i2iHg

- Após o login bem-sucedido, você será redirecionado para a página de inicial da aplicação.
- Na página de listagem de usuários, você poderá visualizar todos os usuários cadastrados.
- Na página de listagem de filmes, você poderá visualizar a lista de filmes disponíveis.

<br>

## 🤝 Dados do Autor

👤 **Jhonatan Sarmento**

- LinkedIn: https://www.linkedin.com/in/jhonatansarmento/
- Github: https://github.com/jhonatansarmento
- Telefone: (92) 99187-0568
