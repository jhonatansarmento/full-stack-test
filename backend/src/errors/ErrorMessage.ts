export enum ErrorMessage {
  INVALID_TOKEN = 'Invalid Token',
  LOGIN_ERROR = 'Invalid Login',
  INVALID_CREDENTIALS = 'Invalid Credentials',
  USER_DATA_ERROR = 'User data error',
  MOVIE_DATA_ERROR = 'Movie data error',
}
