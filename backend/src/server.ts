import 'express-async-errors'
import express from 'express'
import cors from 'cors'
import routes from './routes/routes'
import dotenv from 'dotenv'

dotenv.config()
const app = express()

app.use(cors())
app.use(express.json())
app.use(routes)

app.listen(3333, () => {
  console.warn(`\n[✔] Server iniciado na porta 3333`)
})
