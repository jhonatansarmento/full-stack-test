import { NextFunction, Request, Response } from 'express'
import { AppError } from '../errors/AppError'
import { ListRepository } from '../repositories/ListRepository'
import { UserData } from "../types/UserData";
import { ErrorMessage } from '../errors/ErrorMessage';
import jwt from 'jsonwebtoken'

export default class AccessController {

  async login(request: Request, response: Response): Promise<Response> {
    const { email, password } = request.body

    const listRepository = new ListRepository()
    const data = await listRepository.getUsers()

    const user = data.find((user: UserData) => user?.email?.toLowerCase() === email)
    if (!user || user.password !== password) {
      return response.status(401).json({ message: ErrorMessage.INVALID_CREDENTIALS })
    }

    const token = jwt.sign({ userId: user.id }, String(process.env.JWT_KEY), { expiresIn: '1h' })
    return response.status(200).json({ token })
  } catch(error: any) {
    throw new AppError(`${ErrorMessage.LOGIN_ERROR}: ${error}`)
  }

  static authUserRoute(request: Request, response: Response, next: NextFunction) {
    const token = request?.headers?.authorization?.split(' ')[1] || null
    if (!token) {
      return response.status(401).json({ message: ErrorMessage.INVALID_TOKEN })
    }

    jwt.verify(token, String(process.env.JWT_KEY), (error, decoded) => {
      if (error) {
        return response.status(403).json({ message: ErrorMessage.INVALID_TOKEN })
      }
      request.body.userId = (decoded as any).userId
      next()
    })
  }
}
