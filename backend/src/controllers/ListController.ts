import { Request, Response } from 'express'
import { ListRepository } from '../repositories/ListRepository'
import { ErrorMessage } from '../errors/ErrorMessage'
import { AppError } from '../errors/AppError'

export default class ListController {

  public async listUsers(_request: Request, response: Response): Promise<Response> {
    const listRepository = new ListRepository()
    try {
      const result = await listRepository.getUsers()

      return response.status(200).json(result)
    } catch (error) {
      throw new AppError(ErrorMessage.USER_DATA_ERROR)
    }
  }

  public async listMovies(_request: Request, response: Response): Promise<any> {
    const listRepository = new ListRepository()
    try {
      const result = await listRepository.getMovies()

      return response.status(200).json(result)
    } catch (error) {
      throw new AppError(ErrorMessage.MOVIE_DATA_ERROR)
    }
  }
}
