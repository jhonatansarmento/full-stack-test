import axiosProvider from 'axios';
import { AppError } from '../errors/AppError';
import { MovieData } from "../types/MovieData";
import { UserData } from "../types/UserData";

export class ListRepository {
  async getMovies(): Promise<MovieData[]> {
    try {
      const { data } = await axiosProvider.get(`${String(process.env.API_MOVIES_URL)}&type=movie`);
      return data.Search;
    } catch (error: any) {
      throw new AppError(error.message);
    }
  }

  async getUsers(): Promise<UserData[]> {
    try {
      const { data } = await axiosProvider.get(`${String(process.env.API_USERS_URL)}/api/v1/users`);
      return data;
    } catch (error: any) {
      throw new Error(error.message);
    }
  }
}
