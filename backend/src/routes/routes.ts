import { Router } from 'express'
import ListController from '../controllers/ListController'
import AccessController from '../controllers/AccessController'

const routes = Router()
const accessController = new AccessController()
const listController = new ListController()

routes.post('/login', accessController.login)
routes.get('/users', AccessController.authUserRoute, listController.listUsers)
routes.get('/movies', AccessController.authUserRoute, listController.listMovies)

export default routes
